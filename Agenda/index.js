import express from 'express'

const app = express()

app.use(express.json())

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

export let persons = [
    {
        id: 1,
        name: "Arto Hellas",
        number: "040-123456"
    },
    {
        id: 2,
        name: "Ada Lovelace",
        number: "39-44-5323523"
    },
    {
        id: 3,
        name: "Dan Abramov",
        number: "12-43-234345"
    },
    {
        id: 4,
        name: "Mary Poppendick",
        number: "39-23-6423122"
    }
]

app.get('/', (request, response) => {
    response.send('<h1>AGENDA</h1>')
})
  
app.get('/api/persons', (request, response) => {
    response.json(persons)
})

app.get('/api/persons/:id', (request, response) => {
    let id = parseInt(request.params.id)
    console.log(persons)
    const person = persons.find(person => person.id === id)
    if (person) {
        response.json(person)
    } else {
        response.status(404).json({"Error":"No se encontró ninguna persona con este ID en la agenda"})
    }
})

app.delete('/api/persons/:id', (request, response) => {
    let id = Number(request.params.id)
    let cantidad_inicial = persons.length
    persons = persons.filter(person => person.id !== id)
    if (cantidad_inicial !== persons.length) {
        response.json({"Correcto":`Se eliminó a la persona con el ID ${id}`})
    } else {
        response.status(404).json({"Error":"No se encontró ninguna persona con este ID en la agenda"})
    }
})

app.get('/info', (request, response) => {
    let cant_persons = persons.length
    let fecha = new Date()
    response.send(`
        <p>Phonebook has info for ${cant_persons} people</p>
        <p>${fecha}</p>
    `)
})

app.post('/api/persons/', (request, response) => {
    const persona = request.body
    if(persona.name && persona.number){
        let nuevo_id = 0
        let verif_nombre = persons.find(person => person.name === persona.name)
        let verif_numero = persons.find(person => person.number === persona.number)
        if(verif_nombre===undefined && verif_numero===undefined){
            while(true){
                nuevo_id = getRandomInt(100000)
                if(persons.find(person => person.id === nuevo_id)){
                    continue
                }else{
                    break
                }
            }
            persons.push({"id": nuevo_id, ...persona})
            response.json({"Correcto":"Se agregó correctamente a la presona"})
        }else{
            if(verif_nombre){
                response.status(406).json({"Error":"El valor ingresado en name debe ser único"})
            }
            else if(verif_numero){
                response.status(406).json({"Error":"El valor ingresado en number debe ser único"})
            }
        }
    }else{
        if(persona.name){
            response.status(406).json({"Error":"No se ingresó el valor de name"})
        }
        else if(persona.number){
            response.status(406).json({"Error":"No se ingresó el valor de number"})
        }
        response.status(406).json({"Error":"No se ingresó ningún valor"})
    }
})

if(process.env.NODE_ENV != "test"){
    const PORT = 3001
    app.listen(PORT, () => {
        console.log(`Server running on port ${PORT}`)
    })
}

export default app