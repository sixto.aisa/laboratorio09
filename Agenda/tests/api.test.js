import supertest from 'supertest'
import app,{persons} from '../index'
import 'regenerator-runtime/runtime'

const api = supertest(app)
const initialPersons = [...persons]

const restaurarBD = () => {
  //Borrando todas las personas
  while(persons.length > 0) {
    persons.pop();
  }
  //Agregando nuevamente todas las personas para evitar márgenes de error
  initialPersons.map((person)=>{
    persons.push(person)
  })
}

beforeEach(()=>{
  restaurarBD()
})

describe( 'Tests a la ruta / - Método GET', () => {
  test('Ruta principal retorna contenido HTML', async () =>{
    await api
      .get('/')
      .expect(200)
      .expect('Content-Type',/text\/html/)
  })
});

describe( 'Tests a la ruta /info - Método GET', () => {
  test('Ruta principal retorna contenido HTML', async () =>{
    await api
      .get('/')
      .expect(200)
      .expect('Content-Type',/text\/html/)
  })
});

describe( 'Tests a la ruta /api/persons - Método GET', () => {
  test('Personas son retornadas en formato JSON', async () =>{
    await api
      .get('/api/persons')
      .expect(200)
      .expect('Content-Type',/application\/json/)
  })
  
  test('Hay 4 personas', async () =>{
    const response = await api.get('/api/persons')
    expect(response.body).toHaveLength(initialPersons.length)
  })
  
  test('La primera persona se llama Arto Hellas', async () =>{
    const response = await api.get('/api/persons')
    expect(response.body[0].name).toBe("Arto Hellas")
  })
  
  test('Existe una persona con el nombre Ada Lovelace', async () =>{
    const response = await api.get('/api/persons')
    const nombres = response.body.map((person)=>person.name)
    expect(nombres).toContain("Ada Lovelace")
  })
});

describe( 'Tests a la ruta /api/persons/ - Método POST', () => {
  test('Añadir a una persona - Campos repetidos (nombre)', async () =>{
    const newPerson = {
      name: "Arto Hellas",
      number: "957786155"
    }
    await api.post('/api/persons')
      .send(newPerson)
      .expect(406)
      .expect('Content-Type',/application\/json/)

    const response = await api.get('/api/persons')
    expect(response.body).toHaveLength(initialPersons.length)
  })

  test('Añadir a una persona - Campos repetidos (numero)', async () =>{
    const newPerson = {
      name: "Javier Flores",
      number: "040-123456"
    }
    await api.post('/api/persons')
      .send(newPerson)
      .expect(406)
      .expect('Content-Type',/application\/json/)

    const response = await api.get('/api/persons')
    expect(response.body).toHaveLength(initialPersons.length)
  })

  test('Añadir a una persona - Campos faltantes (nombre)', async () =>{
    const newPerson = {
      number: "957786155"
    }
    await api.post('/api/persons')
      .send(newPerson)
      .expect(406)
      .expect('Content-Type',/application\/json/)

    const response = await api.get('/api/persons')
    expect(response.body).toHaveLength(initialPersons.length)
  })

  test('Añadir a una persona - Campos faltantes (numero)', async () =>{
    const newPerson = {
      name: "Javier Flores"
    }
    await api.post('/api/persons')
      .send(newPerson)
      .expect(406)
      .expect('Content-Type',/application\/json/)

    const response = await api.get('/api/persons')
    expect(response.body).toHaveLength(initialPersons.length)
  })
  
  test('Añadir a una persona - Campos correctos', async () =>{
    const newPerson = {
      name: "Javier Flores",
      number: "957786155"
    }
    await api.post('/api/persons')
      .send(newPerson)
      .expect(200)
      .expect('Content-Type',/application\/json/)

    const response = await api.get('/api/persons')
    expect(response.body).toHaveLength(initialPersons.length + 1)
  })
});

describe( 'Tests a la ruta /api/persons/:id - Método GET', () => {
  restaurarBD()
  test('Retorno de la información de una persona - ID correcto', async () =>{
    await api
      .get('/api/persons/1')
      .expect(200)
      .expect('Content-Type',/application\/json/)
  })

  test('Retorno de la información de una persona - ID incorrecto', async () =>{
    await api
      .get('/api/persons/1000')
      .expect(404)
      .expect('Content-Type',/application\/json/)
  })
});

describe( 'Tests a la ruta /api/persons/:id - Método DELETE', () => {
  restaurarBD()
  test('Eliminar una persona - ID correcto', async () =>{
    await api
      .delete('/api/persons/1')
      .expect(200)
      .expect('Content-Type',/application\/json/)
    const response = await api.get('/api/persons')
    expect(response.body).toHaveLength(initialPersons.length - 1)
  })

  test('Eliminar una persona - ID incorrecto', async () =>{
    await api
      .delete('/api/persons/1000')
      .expect(404)
      .expect('Content-Type',/application\/json/)
    const response = await api.get('/api/persons')
    expect(response.body).toHaveLength(initialPersons.length)
  })
});