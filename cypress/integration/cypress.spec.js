describe('Agenda App', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000')
      cy.request('GET', 'http://localhost:8000/data')
    })

    it('Visualización del Título', () => {
      cy.contains('Phonebook')
    })
    it('Renderizado y click del boton Add', () => {
      cy.get('.btn').click()
    })
    
    it('Probando Request Post', () => {
    const contacto = {
        id:Date.now(),
        name: 'Usuario Prueba '+Date.now(),
        phone:'989562365'
      }
      cy.request('POST', 'http://localhost:8000/data', contacto)
    })

    it('Create a Contact', () => {
        cy.get('[placeholder="Name"]').type('Valeria Gutierrez')
        cy.get('[placeholder="Number"]').last().type('989562578')
        cy.get('.btn').click()
        cy.contains('Valeria Gutierrez')
    })
      
    it('Create a Contact - Datos insuficientes de Numero', () => {
        cy.get('[placeholder="Name"]').type('Maria')
        cy.get('.btn').click()
        cy.contains('Complete todos los datos')
    })
    it('Create a Contact - Datos insuficientes de Nombre', () => {
        cy.get('[placeholder="Number"]').last().type('989562578')
        cy.get('.btn').click()
        cy.contains('Complete todos los datos')
    })
    it('Create a Contact - Nombre Duplicado', () => {
        const stub = cy.stub()  
        cy.get('[placeholder="Name"]').type('Mariam Apaza')
        cy.get('[placeholder="Number"]').last().type('989562578')
        cy.get('.btn').click()
        cy.on('window:alert', (str) => {
            expect(str).to.equal(`Mariam Apaza is already added to phonebook`)
          }) 
    })
    it('Find Contact', () => {
        const stub = cy.stub()  
        cy.get('[placeholder="Find"]').type('Dy')
        cy.contains('Dylan Masset')
    })
      
})