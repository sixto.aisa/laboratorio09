const PersonForm = (props) => {
//Funciones de Evento que capturan el valor ingresado en input
const handleNameChange = (event) =>{
    props.onNameChange(event.target.value)
}
const handlePhoneChange = (event) =>{
    props.onPhoneChange(event.target.value)
}

	return (
        <form onSubmit={props.onClick}>
        <div>
            <div>
                Name <input placeholder="Name" name="Name" value={props.newName} onChange={handleNameChange}/>
            </div>
            <div>
                Number <input placeholder="Number" name="Number" value={props.newPhone}  type="number" onChange={handlePhoneChange} />
            </div>
            <div>
                <button class="btn" 
                style={{backgroundColor:'coral', borderRadius:10,marginTop:20, padding:10}}
                type="submit">Add</button>
            </div>
        </div>
        </form>
	)
}	
export default PersonForm